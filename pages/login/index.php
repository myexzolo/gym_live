<?php
include '../../inc/header.php';
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$checked = "";
$user = "";
if(isset($_COOKIE['gymmonkey_user'])) {
  $checked = "checked";
  $user = $_COOKIE['gymmonkey_user'];
}
?>
    <div id="login-content" role="main">
        <?php include '../../inc/menu2.php'; ?>
        <div class="container mobile-logo">
            <div class="row">
                <div class="col-md-12" align="center">
                    <a class="navbar-brand" href="https://gymmonkeybkk.com">
                        <img src="../../images/logo_yms.png" alt="gym logo" width="300">
                    </a>
                </div>
                <div class="col offset-sm-4 col-sm-4" align="center">
                    <div class="form-box">
                        <h2 align="center" style="color:#232323; margin-bottom: 20px;">Member Login</h2>
                        <form id="formLogin" autocomplete="off" novalidate>
                        <!-- <form id="formLogin" autocomplete="off" method="post" action="../ajax/AEDLogin.php" novalidate> -->
                            <div class="form-group">
                                <label for="user">User ID</label>
                                <input type="text" name="user" value="<?= $user ?>" class="form-control" id="user" required data-smk-msg="&nbsp;">
                            </div>
                            <div class="form-group">
                                <label for="pass">Password</label>
                                <input type="password" name="pass" autocomplete="new-password" class="form-control" id="pass" required data-smk-msg="&nbsp;">
                            </div>
                            <div class="form-check" style="margin-bottom: 1rem;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="checkbox" class="form-check-input" id="remember_me"  name="remember_me" <?=$checked ?> value="Y">
                                        <label class="form-check-label" for="remember_me">remember me</label>
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <a herf="#" style="display:none;">forget password?</a>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-danger btn-block">Submit</button>
                            <div class="col-md-12 py-2">
                                <a href="https://gymmonkeybkk.com"><-- กลับไปหน้าเว็บ Gym Monkey</a>
                            </div>
                        </form>
                        <div class="col-md-12 my-2" align="center">
                            <lead class="copyright" style="color: #fff; margin-top: 20px;">Copyright&copy;2020 Gym Monkey</lead>
                        </div>
                    </>
                </div>
            </div>
        </div>
    </div>
  </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../javascript/jquery.min.js"></script>
    <script src="../../javascript/bootstrap.min.js"></script>
    <script src="../../javascript/smoke.js"></script>
    <script src="../../javascript/all.min.js"></script>
    <script src="../../javascript/popper.min.js"></script>

    </body>

</html>
<script src="js/login.js"></script>
