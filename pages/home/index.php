<?php
    include '../../inc/header.php';
    include '../../inc/function/authen.php';
    include '../../inc/function/connect.php';
    include '../../inc/function/mainFunc.php';

    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    /*
    *   default theme.
    */
    include '../../inc/menu.php';
?>
<div clas="main-section">
    <div class="container-fuild vdo-title">
        <div class="row">
            <div class="col-md-12 py-3">
                <div class="embed-responsive embed-responsive-21by9">
                  <iframe  allow="autoplay; fullscreen" width="1280" height="720" id="iframeStreaming"
                      src="//html.login.in.th/flashstreaming/flash_url/vip/gymmonkey/1280/720/1.html"
                      frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowtransparency="true"
                      webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true">
                    </iframe>
                </div>
            </div>
        </div>
        <div id="schedule"></div>
    </div>
</div>
<div id="couseonline"></div>

<?php
    include '../../inc/footer.php';
?>
<script src="js/home.js?v=5"></script>
