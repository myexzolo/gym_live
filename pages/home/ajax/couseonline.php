<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<div clas="main-section">
      <div class="container-fuild couse-online">
          <div class="row">
              <div class="col-md-12 py-5" align="center">
                  <h2>คอร์สออนไลน์ทั้งหมด</h2>
                  <div class="card-deck py-5">
                      <div class="row">
                        <?php
                              $sqls   = "SELECT v.* , e.EMP_NICKNAME
                                         FROM t_vdo v,data_mas_employee e where v.is_active != 'D' and v.emp_code = e.EMP_CODE ORDER BY v.update_date DESC";

                              $querys     = DbQuery($sqls,null);
                              $json       = json_decode($querys, true);
                              $errorInfo  = $json['errorInfo'];
                              $dataCount  = $json['dataCount'];
                              $rows       = $json['data'];

                              for($i=0 ; $i < $dataCount ; $i++) {
                                $src = "https://gymmonkeybkk.com/admin/vdo/".$rows[$i]['path'];

                        ?>
                        <div class="col-6 col-md-6" style="padding-bottom:20px;">
                          <div class="card">
                            <video width="100%" controls>
                              <source src="<?= $src ?>" type="video/mp4">
                            </video>
                            <h5 class="card-title fontMobile1" style="color:#333;padding-top:5px;"><?=$rows[$i]['title'];?></h5>
                              <div class="card-body" style="border-top: 1px solid rgba(0, 0, 0, 0.125);">
                                  <p class="card-text fontMobile2" style="color:#333;text-align: justify;"><?=$rows[$i]['detail'];?></p>
                              </div>
                              <ul class="list-group list-group-flush" style="border-top: 1px solid rgba(0, 0, 0, 0.125);">
                                  <li class="list-group-item fontMobile2" style="color:#333;text-align: left;">ผู้สอน : <?=$rows[$i]['EMP_NICKNAME'];?></li>
                              </ul>
                          </div>
                        </div>
                      <?php } ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
