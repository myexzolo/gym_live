<?php
include '../../inc/header.php';
include '../../inc/function/connect.php';
include '../../inc/function/mainFunc.php';

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
            <div class="row">
                <div class="col-md-12 py-3">
                    <table class="table table-striped table-dark tbl-class">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">เข้าชม</th>
                            <th scope="col">วันที่/เวลา</th>
                            <th scope="col">รายการ</th>
                            <th scope="col">ผู้สอน</th>
                            <th scope="col">จองคลาส</th>
                            <th scope="col">จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $code = "1";
                            $date = date("Y/m/d");
                            // $sql  = "SELECT sd.*,c.name_class,e.EMP_NICKNAME
                            //         FROM tb_schedule_class_day sd,t_classes c, data_mas_employee e
                            //         WHERE sd.id_class = c.id_class and sd.EMP_CODE = e.EMP_CODE
                            //         and sd.date_class >= '$date' and sd.is_live = 'Y'  order by sd.date_class, sd.day, sd.row  Limit 0,20";

                            $sql  = "SELECT sd.*,c.name_class,e.EMP_NICKNAME,r.reserve_id,r.status
                                    FROM tb_schedule_class_day sd
                                    INNER JOIN t_classes c ON sd.id_class = c.id_class
                                    INNER JOIN data_mas_employee e ON sd.EMP_CODE = e.EMP_CODE
                                    left JOIN t_reserve_class r ON sd.id = r.schedule_day_id and r.PERSON_CODE = '$code' and r.status <> ('D')
                                    where sd.date_class >= '$date' and sd.is_live = 'Y' order by sd.date_class, sd.day, sd.time_start Limit 0,20";
                            //echo $sql;

                                    $query      = DbQuery($sql,null);
                                    $json       = json_decode($query, true);
                                    $row        = $json['data'];
                                    $dataCount  = $json['dataCount'];

                                    $dateTime = date("Y/m/d H:i");

                                    for($i=0;$i<$dataCount; $i++)
                                    {
                                      $dateTimeClassStart =  $row[$i]['date_class']." ".$row[$i]['time_start'];
                                      $dateTimeClassEnd   =  $row[$i]['date_class']." ".$row[$i]['time_end'];
                                      $sign_emp           =  $row[$i]['sign_emp'];

                                      $diffStrat  =  DateTimeDiff($dateTime,$dateTimeClassStart);
                                      $diffEnd    =  DateTimeDiff($dateTime,$dateTimeClassEnd);


                                      $diffDay    =  DateTimeDiff($dateTimeClassStart,$dateTimeClassEnd);

                                      //echo ">>".$diffStrat.",".$diffEnd." ,";
                                      $reserve_id = $row[$i]['reserve_id'];
                                      $status     = $row[$i]['status'];
                                      $schedule_day_id = $row[$i]['id'];

                                      $liveIcon = "";
                                      $btl = "";


                                      $img = "<img src='../../images/giphydis.gif' onclick='' style='margin-top: -20px;margin-bottom: -20px;width:50px'>";
                                      $golive = "window.open('https://live.gymmonkeybkk.com/')";

                                      if($diffEnd <= 0)
                                      {
                                        $btl = "";
                                        $img = "";
                                      }else{
                                        if($reserve_id != null && $reserve_id != "" && $status != null && $status != "D"){
                                          $btl="<button class='btn btn-danger btn-sm' onclick=\"cancelClass('$reserve_id')\">ยกเลิก</button>";
                                          if($sign_emp == "Y"){
                                            $img = "../../images/giphy.gif";
                                            //$golive = "golive('$schedule_day_id','$reserve_id')";
                                            $img = "<img src='../../images/giphy.gif' onclick=\"$golive\" style='margin-top: -20px;margin-bottom: -20px;width:50px;'>";
                                          }
                                        }else{
                                          $btl ="<button class='btn btn-primary btn-sm' onclick=\"window.open('https://live.gymmonkeybkk.com/')\">เข้าร่วม</button>";
                                        }
                                      }

                                      // $liveClassId = "";
                                      // if($diffStrat <= 0.05 && $diffEnd >= 0)
                                      // {
                                      //   $liveClassId = $schedule_day_id;
                                      // }
                                      $dateShow = "";
                                      if($diffDay > 8)
                                      {
                                        continue;
                                        $dateShow = "ทั้งวัน";
                                        $person_join = "unlimited";
                                      }else{
                                        $dateShow = DateTimeThai2($dateTimeClassStart);
                                        $person_join  = $row[$i]['person_join']."/".$row[$i]['unit'];
                                      }
                        ?>
                            <tr>
                            <td align="center"><?= $img ?></td>
                            <td><?= $dateShow ?></td>
                            <td><?= $row[$i]['name_class'] ?></td>
                            <td><?= $row[$i]['EMP_NICKNAME'] ?></td>
                            <td align="center"><?= $btl ?></td>
                            <td align="right"><?= $person_join ?></td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>

                    <!-- mobile switch on -->
                    <table class="table table-striped table-dark tbl-class-mobile">
                        <thead class="thead-light">
                            <tr>
                            <th style="width:60px;font-size:13px;">เข้าชม</th>
                            <th style="font-size:13px;">รายการ</th>
                            <th style="width:80px;font-size:13px;">เข้าร่วม</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php

                              for($i=0;$i<$dataCount; $i++)
                              {
                                $dateTimeClassStart =  $row[$i]['date_class']." ".$row[$i]['time_start'];
                                $dateTimeClassEnd   =  $row[$i]['date_class']." ".$row[$i]['time_end'];
                                $sign_emp           =  $row[$i]['sign_emp'];

                                $diffStrat  =  DateTimeDiff($dateTime,$dateTimeClassStart);
                                $diffEnd    =  DateTimeDiff($dateTime,$dateTimeClassEnd);

                                $diffDay    =  DateTimeDiff($dateTimeClassStart,$dateTimeClassEnd);

                                if($diffDay > 8)
                                {
                                  continue;
                                }

                                //echo ">>".$diffStrat.",".$diffEnd." ,";
                                $reserve_id = $row[$i]['reserve_id'];
                                $status     = $row[$i]['status'];
                                $schedule_day_id = $row[$i]['id'];

                                $liveIcon = "";
                                $btl = "";

                                $img = "../../images/giphydis.gif";

                                //$sign_emp = "Y";
                                $golive = "window.open('https://live.gymmonkeybkk.com/')";

                                if($diffEnd <= 0)
                                {
                                  $btl = "";
                                }else{
                                  if($reserve_id != null && $reserve_id != "" && $status != null && $status != "D"){
                                    $btl="<button class='btn btn-danger btn-sm' style='font-size:12px;' onclick=\"cancelClass('$reserve_id')\">ยกเลิก</button>";
                                    if($sign_emp == "Y"){
                                      $img = "../../images/giphy.gif";
                                      //$golive = "window.open('https://live.gymmonkeybkk.com/');";
                                    }
                                  }else{
                                    $btl ="<button class='btn btn-primary btn-sm' style='font-size:12px;' onclick=\"window.open('https://live.gymmonkeybkk.com/')\">เข้าร่วม</button>";
                                  }
                                }
                        ?>
                            <tr>
                            <td>
                              <img src='<?= $img ?>' onclick="<?= $golive?>" style='margin-top: -20px;margin-bottom: -20px;width:40px'>
                            </td>
                            <td>
                              <div class="row">
                                <div class="col-md-12" style="font-size:13px;"><b style="color:#ff9f82"><?= $row[$i]['name_class']?></b></div>
                                <div style="padding-top:20px;"></div>
                                <div class="col-md-12" style="font-size:10px;color:#ccc"><?= DateTimeThai2($dateTimeClassStart)." - ".$row[$i]['time_end'] ?></div>
                                <div class="col-md-12" >
                                  <div style="font-size:12px;width:70%;float: left;"><?= $row[$i]['EMP_NICKNAME'] ?></div>
                                  <div style="font-size:10px;width:30%;text-align:right;float: right;"><?= $row[$i]['person_join']."/".$row[$i]['unit']; ?></div>
                                </div>

                              </div>
                            </td>
                            <td><div align="center"><?= $btl ?></div></td>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="../../javascript/jquery.min.js"></script>
            <script src="../../javascript/bootstrap.min.js"></script>
            <script src="../../javascript/smoke.js"></script>
            <script src="../../javascript/all.min.js"></script>
            <script src="../../javascript/popper.min.js"></script>
            </body>

        </html>
