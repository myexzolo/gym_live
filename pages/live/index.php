<?php
    include '../../inc/header.php';
    include '../../inc/function/authen.php';
    include '../../inc/function/connect.php';
    include '../../inc/function/mainFunc.php';

    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    /*
    *   default theme.
    */
    $id = isset($_GET['id'])?$_GET['id']:"";
    if($id == "")
    {
        exit("<script>window.location='../../pages/home/'</script>");
    }
    include '../../inc/menu.php';
    include 'inc/livestream.php';
    include '../../inc/footer.php';

?>
<style>

@media (max-width: 1200px) {
  html {
    overflow-x: hidden;
  }

  body {
    overflow-x: hidden;
  }
  .responsive{
    display:none !important;
  }
  .vdo-title {
    padding-left: 0px;
    padding-right: 0px;
  }
  .chatMobile{
    padding-top: 0px !important;
    padding-bottom: 0px !important;
  }
  .box {
    position: relative;
    border-radius: 0px;
    background: #232323;
    border-top: 0px;
    margin-bottom: 0px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
  }

  .direct-chat-name{
    color:#fff;
  }
}

.btn-warning:focus, .btn-warning.focus {
  color: #212529;
  background-color: #ffc107;
  border-color: #ffc107;
  box-shadow: 0 0 0 0 !important;

}

.btn-warning {
    color: #212529;
    background-color: #ffc107;
}
.btn-warning:hover {
    color: #212529;
    background-color: #e0a800;
    /* border-color: #d39e00; */
}



</style>
<script src="js/live.js?v=5"></script>
