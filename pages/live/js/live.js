var scrollTop = null;
var scrollHeight = 0;

$(function(){

  run();
  chatList();
  checkLive();
  setInterval(function(){
    run();
    chatList();
    checkLive();
  }, 3000);
  var chatMessages = $("#chatMessages");
  var vdoFrame      = $("#vdoFrame");

  var $window = $(window).on('resize', function(){
     // alert($(this).height());
     // var res = $()
     var heightVdoFrame = vdoFrame.height();
     var height = $(this).height();
     //console.log(height);
     if(heightVdoFrame > 300){
       height = heightVdoFrame;
     }else{
       height -= (heightVdoFrame + 150);
     }
     console.log(height);
     chatMessages.height(height);
  }).trigger('resize'); //on page load

});



function run(){
  $.get( "../../inc/function/checkToken.php")
  .done(function( data ) {
    //console.log(data);
    if(data.status){
      window.location = '../../pages/login/';
    }
  });
}

var idScheduleClassDay = "xxxx";
function checkLive(){
  var url = "inc/checkLive.php?id="+idScheduleClassDay;
  $.get(url)
  .done(function( data ) {
    if(data.status){
       var id = data.id;
       if(id !="" && idScheduleClassDay != id){
         console.log(data.id+", "+idScheduleClassDay+", "+data.type);
         idScheduleClassDay = id;
         $('#iframeStreaming').attr('src',"//html.login.in.th/flashstreaming/flash_url/vip/gymmonkey/1280/720/1.html");
       }
    }
  });
}

function chatList()
{
  var id = $("#schedule_class_id").val();
  //console.log(id);
  $.post( "inc/chat.php",{id:id})
  .done(function( data ) {
    $("#chatMessages").html(data);
    var chatHistory = document.getElementById("chatMessages");
    //console.log(chatHistory.scrollTop +" "+ chatHistory.scrollHeight+" "+ scrollTop);
    if(scrollTop == null){
      chatHistory.scrollTop = chatHistory.scrollHeight;
      scrollTop = chatHistory.scrollTop;
      scrollHeight = chatHistory.scrollHeight;
    }
    if(chatHistory.scrollTop > scrollTop && chatHistory.scrollHeight > scrollHeight ){
      chatHistory.scrollTop = chatHistory.scrollHeight;
      scrollTop = chatHistory.scrollTop;
      scrollHeight = chatHistory.scrollHeight;
    }
    if(chatHistory.scrollTop == scrollTop && chatHistory.scrollHeight > scrollHeight ){
      chatHistory.scrollTop = chatHistory.scrollHeight;
      scrollTop = chatHistory.scrollTop;
      scrollHeight = chatHistory.scrollHeight;
    }

    //chatHistory.scrollTop = chatHistory.scrollHeight;
  });
}


$('#formChat').on('submit', function(event) {
  event.preventDefault();
  if ($('#formChat').smkValidate()) {
    $.ajax({
        url: 'inc/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        $('#message').val("");
        if(data.status){
          chatList();
        }
    });
  }
});
