<?php

$sql  = "SELECT s.*,c.name_class,c.detail_class FROM tb_schedule_class_day  s, t_classes c
         where s.id = '$id' and s.id_class = c.id_class";
//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$dateTime = date("Y/m/d H:i");

for($i=0;$i<$dataCount; $i++)
{
  $name_class         =  $row[$i]['name_class'];
  $detail_class       =  $row[$i]['detail_class'];
  $sign_emp           =  $row[$i]['sign_emp'];

  $dateTimeClassStart =  $row[$i]['date_class']." ".$row[$i]['time_start'];
  $dateTimeClassEnd   =  $row[$i]['date_class']." ".$row[$i]['time_end'];

  $diffStrat  =  DateTimeDiff($dateTime,$dateTimeClassStart);
  $diffEnd    =  DateTimeDiff($dateTime,$dateTimeClassEnd);
  //echo $diffStrat." ".$diffEnd;
  //$sign_emp = 'E';
  if($sign_emp == "N")
  {
    exit("<script>window.location='../../pages/home/'</script>");
  }

}

?>
    <div clas="main-section">
        <div class="container-fuild vdo-title">
            <div class="row">
                <div class="col-md-8 col-lg-8 py-3">
                    <div id="vdoFrame">
                      <?php
                        if($sign_emp == "Y")
                        {
                      ?>

                      <div class="embed-responsive embed-responsive-16by9">
                      <iframe  allow="autoplay; fullscreen" width="1280" height="720" id="iframeStreaming"
                          src="//html.login.in.th/flashstreaming/flash_url/vip/gymmonkey/1280/720/1.html"
                          frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowtransparency="true"
                          webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true">
                        </iframe>
                      </div>
                      <?php
                        }else{
                      ?>
                      <div>
                        <img src="../../images/offline.png"  style="display:block;width:100%; height:100%;object-fit: cover">
                      </div>
                      <?php
                        }
                      ?>
                      </div>
                    <div class="row responsive">
                        <div class="col-md-2"><img src="../../images/giphy.gif" alt="gym logo" style="margin-top: 10px;margin-bottom: 10px;width:60px"></div>
                        <div class="col-md-10"><h2 style="margin-top: 20px; color: #fff;"><?= $name_class?></h2>
                          <p style="color: #fff;"><?= $detail_class ?></p>
                          </div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-md-start responsive">
                        <?php
                            $sqlp  = "SELECT r.*,p.PERSON_NAME,p.PERSON_LASTNAME,p.PERSON_NICKNAME FROM t_reserve_class r, person p
                                     where r.schedule_day_id = '$id' and r.PERSON_CODE = p.PERSON_CODE and r.status != 'D'";
                            //echo $sqlp;

                            $queryp      = DbQuery($sqlp,null);
                            $jsonp       = json_decode($queryp, true);
                            $rowp        = $jsonp['data'];
                            $dataCountp  = $jsonp['dataCount'];

                            //btn-danger
                            for($j=0;$j<$dataCountp; $j++)
                            {
                                $nickname = $rowp[$j]['PERSON_NICKNAME'];
                                $status   = $rowp[$j]['status'];

                                $btnStatus = " btn-danger ";
                                if($status == "C")
                                {
                                  $btnStatus = " btn-success ";
                                }
                                echo "<button class='btn $btnStatus user-online btn-sm'><i class='fas fa-user-alt'></i> $nickname</button>";
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 py-3 chatMobile">
                    <div class="row">
                      <div class="col-md-12">
                            <!-- DIRECT CHAT -->
                            <div class="box direct-chat direct-chat-warning" >
                              <!-- /.box-header -->
                              <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages" id="chatMessages"></div>
                              </div>
                              <!-- /.box-body -->
                              <div class="box-footer">
                                <form id="formChat" novalidate>
                                  <div class="input-group">
                                    <input type='hidden' value='<?=$id ?>' id='schedule_class_id' name="schedule_class_id">
                                    <input type="text" name="message" id="message" placeholder="ข้อความ ..." class="form-control" required>
                                    <span class="input-group-btn">
                                          <button type="submit" class="btn btn-warning btn-flat"  style="border-radius: 0;">Send</button>
                                        </span>
                                  </div>
                                </form>
                              </div>
                              <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
