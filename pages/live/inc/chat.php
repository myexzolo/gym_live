<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

// $schedule_day_id    = "522020040351";
// $code               = "190502001";

$schedule_day_id     = isset($_POST['id'])?$_POST['id']:"";
$code                = $_SESSION['person']['code'];

$sql   = "SELECT * FROM t_chat where schedule_day_id = '$schedule_day_id' order by create_date ASC";
//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

//btn-danger
for($x=0;$x<$dataCount; $x++)
{
  $codeId       = $row[$x]['code'];
  $create_date  = DateTimeThai($row[$x]['create_date']);
  $message      = $row[$x]['message'];
  $nickname     = $row[$x]['name'];
  $image        = $row[$x]['img'];

  $image = trim(str_replace("data:image/png;base64,","",$image));

  if($image != "" && $image != null)
  {
    $image    = "data:image/png;base64, ".$image;
  }else{
    $image = "../../images/user.png";
  }

  //echo $PERSON_NICKNAME;
  $my = "right";
  $chatNamePull   = "pull-right";
  $chatTimestamp  = "pull-left";
  if($codeId  == $code){
    $my = "";
    $chatNamePull   = "pull-left";
    $chatTimestamp  = "pull-right";
  }
?>
<div class="direct-chat-msg <?= $my ?>">
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-name <?= $chatNamePull ?>"><?= $nickname ?></span>
    <span class="direct-chat-timestamp <?= $chatTimestamp ?>"><?=$create_date  ?></span>
  </div>
  <img class="direct-chat-img" src="<?= $image ?>" onerror="this.onerror='';this.src='../../images/user.png'" alt="message user image">
  <div class="direct-chat-text"><?= $message ?></div>
</div>
<?php
}
?>
