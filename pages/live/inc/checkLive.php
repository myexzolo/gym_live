<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

if(!isset($_SESSION))
{
    session_start();
}

$id_tmp = isset($_GET['id'])?$_GET['id']:"";
$date   = date('Y/m/d');
$time   = date('H:i');
$sql   = "SELECT * FROM tb_schedule_class_day where date_class = '$date'  and is_live ='Y' and time_start <= '$time' && time_end > '$time' order by time_start desc ";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$id   = "";
$type = "";
if($dataCount > 0)
{
  $dateTimeClassStart =  $row[0]['date_class']." ".$row[0]['time_start'];
  $dateTimeClassEnd   =  $row[0]['date_class']." ".$row[0]['time_end'];

  $diffDay    =  DateTimeDiff($dateTimeClassStart,$dateTimeClassEnd);

  if($diffDay < 8)
  {
      $id  = $row[0]['id'];
      $type = "online";
  }else if($diffDay > 8 &&  $id_tmp != "" && $id_tmp != $id){
     $id  = $row[0]['id'];
     $type = "offline";
  }
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $dataCount,'id'=> $id,'type'=> $type,'sql'=> $sql)));
?>
