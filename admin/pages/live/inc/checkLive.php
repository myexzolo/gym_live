<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

$date   = date('Y/m/d');
$time   = date('H:i');
$sql   = "SELECT * FROM tb_schedule_class_day where date_class = '$date'  and is_live ='Y' and time_start = '$time'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$id   = "";
if($dataCount > 0)
{
  $id  = $row[0]['id'];
}


header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $dataCount,'id'=> $id,'sql'=> $sql)));
?>
