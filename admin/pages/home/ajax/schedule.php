<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
?>
            <div class="row">
                <div class="col-md-12 py-3">
                    <table class="table table-striped table-dark tbl-class">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">Live</th>
                            <th scope="col">วันที่/เวลา</th>
                            <th scope="col">รายการ</th>
                            <th scope="col">ผู้สอน</th>
                            <th scope="col">จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $code = $_SESSION['emp']['code'];
                            $date = date("Y/m/d");
                            // $sql  = "SELECT sd.*,c.name_class,e.EMP_NICKNAME
                            //         FROM tb_schedule_class_day sd,t_classes c, data_mas_employee e
                            //         WHERE sd.id_class = c.id_class and sd.EMP_CODE = e.EMP_CODE
                            //         and sd.date_class >= '$date' and sd.is_live = 'Y'  order by sd.date_class, sd.day, sd.row  Limit 0,20";

                            $sql  = "SELECT sd.*,c.name_class,e.EMP_NICKNAME
                                    FROM tb_schedule_class_day sd
                                    INNER JOIN t_classes c ON sd.id_class = c.id_class
                                    INNER JOIN data_mas_employee e ON sd.EMP_CODE = e.EMP_CODE
                                    where sd.date_class >= '$date' and sd.is_live = 'Y' order by sd.date_class, sd.day, sd.time_start  Limit 0,20";
                            //echo $sql;

                                    $query      = DbQuery($sql,null);
                                    $json       = json_decode($query, true);
                                    $row        = $json['data'];
                                    $dataCount  = $json['dataCount'];

                                    $dateTime = date("Y/m/d H:i");

                                    for($i=0;$i<$dataCount; $i++)
                                    {
                                      $dateTimeClassStart =  $row[$i]['date_class']." ".$row[$i]['time_start'];
                                      $dateTimeClassEnd   =  $row[$i]['date_class']." ".$row[$i]['time_end'];
                                      $sign_emp           =  $row[$i]['sign_emp'];

                                      $diffStrat  =  DateTimeDiff($dateTime,$dateTimeClassStart);
                                      $diffEnd    =  DateTimeDiff($dateTime,$dateTimeClassEnd);


                                      $schedule_day_id = $row[$i]['id'];

                                      $liveIcon = "";
                                      $btl = "";

                                      $img = "<img src='../../images/giphydis.gif' onclick='' style='margin-top: -20px;margin-bottom: -20px;width:50px'>";
                                      $golive = "";
                                      //$sign_emp = "Y";

                                      // $liveClassId = "";
                                      //echo $diffStrat.", ".$diffEnd;
                                      if($diffStrat <= 0.5 && $diffEnd >= 0)
                                      {
                                        $golive = "golive('$schedule_day_id','$code')";
                                        $img = "<img src='../../images/giphy.gif' onclick=\"$golive\" style='margin-top: -20px;margin-bottom: -20px;width:50px'>";

                                      }else if($diffEnd <= 0)
                                      {
                                        $img = "";
                                        $golive = "";
                                      }
                        ?>
                            <tr>
                            <td align="center"><?= $img ?></td>
                            <td><?= DateTimeThai2($dateTimeClassStart) ?></td>
                            <td><?= $row[$i]['name_class'] ?></td>
                            <td><?= $row[$i]['EMP_NICKNAME'] ?></td>
                            <td align="right"><?= $row[$i]['person_join']."/".$row[$i]['unit']; ?></td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>

                    <!-- mobile switch on -->
                    <table class="table table-striped table-dark tbl-class-mobile">
                        <thead class="thead-light">
                            <tr>
                            <th>live</th>
                            <th>รายการ</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                              for($i=0;$i<$dataCount; $i++)
                              {
                                $dateTimeClassStart =  $row[$i]['date_class']." ".$row[$i]['time_start'];
                                $dateTimeClassEnd   =  $row[$i]['date_class']." ".$row[$i]['time_end'];
                                $sign_emp           =  $row[$i]['sign_emp'];

                                $diffStrat  =  DateTimeDiff($dateTime,$dateTimeClassStart);
                                $diffEnd    =  DateTimeDiff($dateTime,$dateTimeClassEnd);


                                $schedule_day_id = $row[$i]['id'];

                                $liveIcon = "";
                                $btl = "";

                                $img = "<img src='../../images/giphydis.gif' onclick='' style='margin-top: -20px;margin-bottom: -20px;width:50px'>";
                                $golive = "";
                                //$sign_emp = "Y";

                                // $liveClassId = "";
                                //echo $diffStrat.", ".$diffEnd;
                                if($diffStrat <= 0.5 && $diffEnd >= 0)
                                {
                                  $golive = "golive('$schedule_day_id','$code')";
                                  $img = "<img src='../../images/giphy.gif' onclick=\"$golive\" style='margin-top: -20px;margin-bottom: -20px;width:50px'>";

                                }else if($diffEnd <= 0)
                                {
                                  $img = "";
                                  $golive = "";
                                }
                        ?>
                            <tr>
                            <td width="60"><?= $img ?></td>
                            <td>
                              <div class="row">
                                <div class="col-md-12" style="font-size:13px;"><b style="color:#ff9f82"><?= $row[$i]['name_class']?></b></div>
                                <div style="padding-top:20px;"></div>
                                <div class="col-md-12" style="font-size:10px;color:#ccc"><?= DateTimeThai2($dateTimeClassStart)." - ".$row[$i]['time_end'] ?></div>
                                <div class="col-md-12" >
                                  <div style="font-size:12px;width:70%;float: left;"><?= $row[$i]['EMP_NICKNAME'] ?></div>
                                  <div style="font-size:10px;width:30%;text-align:right;float: right;"><?= $row[$i]['person_join']."/".$row[$i]['unit']; ?></div>
                                </div>

                              </div>

                            </td>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
