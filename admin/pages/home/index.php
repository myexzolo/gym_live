<?php
    include '../../inc/header.php';
    include '../../inc/function/authen.php';
    include '../../inc/function/connect.php';
    include '../../inc/function/mainFunc.php';

    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    /*
    *   default theme.
    */
    include '../../inc/menu.php';
?>
<div clas="main-section">
    <div class="container-fuild vdo-title">
        <div id="schedule"></div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../javascript/jquery.min.js"></script>
<script src="../../javascript/bootstrap.min.js"></script>
<script src="../../javascript/smoke.js"></script>
<script src="../../javascript/all.min.js"></script>
<script src="../../javascript/popper.min.js"></script>
<script>
function logout(){
  $.smkConfirm({
    text:'ยืนยันการออกจากระบบ ?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      console.log("xxx");
      $.get( "../../inc/function/logout.php")
      .done(function( data ) {
        console.log(data);
        if(data.status){
          window.location='../../pages/login/';
        }

      });

    }
  });
}
</script>
</body>

</html>
<script src="js/home.js"></script>
