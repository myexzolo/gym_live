<?php
include '../../inc/header.php';
include '../../inc/function/authen.php';
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$checked = "";
$user = "";
if(isset($_COOKIE['gymmonkey_user'])) {
  $checked = "checked";
  $user = $_COOKIE['gymmonkey_user'];
}
?>
<style>
::-webkit-input-placeholder { /* Edge */
  font-size: 14px;
}

:-ms-input-placeholder { /* Internet Explorer */
  font-size: 14px;
}

::placeholder {
  font-size: 14px;
}
</style>
    <div id="login-content" role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12" align="center">
                    <a class="navbar-brand" href="https://gymmonkeybkk.com">
                        <img src="../../images/logo_yms.png" alt="gym logo" width="300">
                    </a>
                </div>
                <div class="col-md-12 p-2" align="center">
                    <div class="form-box">
                        <h2 align="center" style="color: #232323; margin-bottom: 30px;">กรุณาเปลี่ยน Password</h2>
                        <form id="form" autocomplete="off" novalidate>
                        <!-- <form id="form" autocomplete="off" method="post" action="ajax/AED.php" novalidate> -->
                            <div class="form-group">
                                <label for="user">New Password</label>
                                <input type="password" name="pass"  value="" autocomplete="new-password"
                                data-smk-strongPass="weak" placeholder="ภาษาอังกฤษหรือตัวเลข ไม่น้อยกว่า 6 อักษร" class="form-control" id="pass" required data-smk-msg="&nbsp;">
                            </div>
                            <div class="form-group">
                                <label for="pass">Confirm Password</label>
                                <input type="password" name="pass2" autocomplete="new-password" class="form-control" id="pass2" required data-smk-msg="&nbsp;">
                            </div>
                            <div class="form-check" style="margin-bottom: 1rem;">
                                <div class="row">
                                    <div class="col-md-6" align="right">
                                        <a herf="#" style="display:none;">forget password?</a>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-danger btn-block">บันทึก</button>
                        </form>
                        <div class="col-md-12" align="center">
                            <lead class="copyright">Copyright&copy;2020 Gym Monkey</lead>
                        </div>
                    </div>
                    <p class="p-2">กรุณาเปลี่ยน Password สำหรับการเข้าใช้งานใหม่</p>
                </div>
            </div>
        </div>
    </div>
  </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../javascript/jquery.min.js"></script>
    <script src="../../javascript/bootstrap.min.js"></script>
    <script src="../../javascript/smoke.js"></script>
    <script src="../../javascript/all.min.js"></script>
    <script src="../../javascript/popper.min.js"></script>

    </body>

</html>
<script src="js/reset.js"></script>
