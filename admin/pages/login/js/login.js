
// JS Login //
$('#formLogin').on('submit', function(event) {
  event.preventDefault();
  if ($('#formLogin').smkValidate()) {
    $.ajax({
        url: 'ajax/AEDLogin.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {

      // $.smkProgressBar({
      //   element:'body',
      //   status:'start',
      //   bgColor: '#000',
      //   barColor: '#fff',
      //   content: 'Loading...'
      // });
      // setTimeout(function(){
        // $.smkProgressBar({status:'end'});
        //$('#formLogin').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == 'success'){
          window.location = '../../pages/home/';
        }
      // }, 1000);
    });
  }
});
