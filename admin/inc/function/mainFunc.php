<?php


function removePage($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($dir."/".$object) == "dir")
           removePage($dir."/".$object);
        else unlink   ($dir."/".$object);
      }
    }
    reset($objects);
    rmdir($dir);
  }
 }


 function getRealIP()
    {
      $ipaddress = '';
      if (getenv('HTTP_CLIENT_IP'))
          $ipaddress = getenv('HTTP_CLIENT_IP');
      else if(getenv('HTTP_X_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
      else if(getenv('HTTP_X_FORWARDED'))
          $ipaddress = getenv('HTTP_X_FORWARDED');
      else if(getenv('HTTP_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_FORWARDED_FOR');
      else if(getenv('HTTP_FORWARDED'))
         $ipaddress = getenv('HTTP_FORWARDED');
      else if(getenv('REMOTE_ADDR'))
          $ipaddress = getenv('REMOTE_ADDR');
      else
          $ipaddress = 'UNKNOWN';
      return $ipaddress;
    }

function randomString($length = 4 , $type = 0) {
  // type 0 = UPPER STRING , 1 = LOWER STRING , 2 = MATCH ,
  // 3 = UPPER & LOWER STRING , 4 UPPER & LOWER & MATCH
  if($type == 0){
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }else if($type == 1){
    $characters = 'abcdefghijklmnopqrstuvwxyz';
  }else if($type == 2){
    $characters = '0123456789';
  }else if($type == 3){
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }else if($type == 4){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function getOTP(){
  return  array('REF' => randomString($length = 5 , $type = 0) , 'OTP' => randomString($length = 4 , $type = 2) );
}

function DateTimeThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		return "$strDay/$strMonth/$strYear $strHour:$strMinute:$strSeconds";
}

function DateTimeThai2($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		return "$strDay/$strMonth/$strYear $strHour:$strMinute";
}

function DateTimeThaiS2($strDate){
		$strYear = date("y",strtotime($strDate))+43;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		return "$strDay/$strMonth/$strYear $strHour:$strMinute";
}

function DateThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

		return "$strDay/$strMonth/$strYear";
}

function DateEn($strDate){
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

		return "$strDay/$strMonth/$strYear";
}

function dateThToEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex]);
    return $dateEN;
}


function datetimeEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatDateTime   = explode(" ",$date);//var formatItems=formatLowerCase.split(delimiter);
    $date             = $formatDateTime[0];
    $time             = $formatDateTime[1];

    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex])." ".$time;
    return $dateEN;
}





function signature(){
  return $str = '<p><br />
  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAgAElEQVR4Xu2cB5hV5bX+f7udM4UytKEjIkbFAkhQDNZEjS0WUDQqoAiiQgQEBaOioKLRWLGgEkssSSyJ3pgbb4zGjmLsEBVFijDCwPRyym73WevbZ0AUTBD533v/2TzzMHPKPnu/3yrvetf6jhVHQcz2POKI2LKxIhusmNiK5T9zyN/YWMgDAWBD7BJZEFshVmzpY3YU6WvBkTcBhb+3z41Y2x80uUk5YiI70lu2IvnLwsIltn35AyvywBZQA4OPgmwrPvqAFRHb8oeAXgDv/yxotlqX3L2FWJ0BTCzGYBATOTa2WpE8F+hjCowAJe8US40F5ACLEHATi/s/C5qlFqYuhgAoriqgxcR2iGV5eudRUwNxGOO0aWPAi2KsKCR2I+JYXNg1XllwzYIBbwfctr97CgBWhBXHGq8UALEuVyC0yC7/mDW/eZiGjz6EIKao2450PmUYbQbtB6EArUgR2fK+JLS1BMXtgJhGh+2dCNS9jJs5kYBmETmBOmPNn59m+S23wbIleJ6NG1nkg4hcp070GH8OXUedTkwaKxS0QiJb3Fbc1N4+aLXkq+0NWmxpJpRYZod2Er9C1j78a1bedAMlmZB0qlgTgCZVsap8jgZCOo89i14TpxE7Kawo0HMIZF865E3fsatuX0vTqC9xTIJ3iGWLfTmsfWQ+K6+/gdZhhJ1KiSkq7VCGodFODCtPY8an03nn0XPSJMBT942d0ABXAEvfJAsjIUBcedtb4fYFTREQ81COAbbL2t8/woor59A2CLFTrrGuTQ6NXZI0goBa8nS5YDI9z5hAFAt3MzGu5dRKYZwEcJOXt/Wx3UGLlGiBbXnUvfIcn06dSnFzDiftJOT1a0CTHCkWagfg52i0PXrPnk3Hn5xIHIZEjoAjtDhOeJyQYvmRGGA+b1se2x20WKzDTpH9+EMWTT6H0pUVuOk0kVQGmwnoUifI87YE/dgmymep79yO3W68hdb9h5isKslYqJxYHpFmV1sIsTLlbQnZ/4PsGVsOcU0NH02dhP/6S6RLWit1EEAM6f36Q8AQwHzHxolj/KYs4YDd2e2Wm3E77wC+nMRYlmUFSp1tsbbv4NhOlhYLqU8Cf8zSOVdT/eD9tCotUbcKLRsnUqq7xVsUixOzcSM5mUNjYxOlw4+h79VziOw0diiJRSoKU3oJyNvayuQCtwNolrJ5yWa27bHmyd+zfNZs2joBse1qNeBGgXK3zbmnelcMgSV1gLhfQGhZ2IFDbeTT66KL6XL6SPw4xotsQifCltLrO+Jv3w1o6mqyIpoiiWILYReZJe/z/sTJpCorSaU8ecaEHI1DWp/rUfDSAuWQxwRSo3HEUrGa12ET5LNk25Wx6x23UbLHPhCGaoXyDinojTKybY/vCDRxi0BdMrIsLMvFbm7g/akX0vzaK5SWpLGiGCeSG5QILvco2W/z3iTPOpERjYyqJDCKUgLZ5gZSg3/ArrfdhdW6NUQiKxng/ve4p65umJRLDo7l8und81g7725apT1DDyLhWKGJY6r0CBwFvePrLCMyuproaVo6mdfakaWu2pBtoHzSOfQcP83wN1ksjX3bPhl8R5Ymd6dEAcvxqHvn77xx4TTKmrK4IvtEIXYY4oiVye9J4a10RG5W49Gmh3FLgcvEOHFOUwnIe0I/S1Opy/fmzqd08H7EsZ+88n+Re2oZYzmEdfW8PmM6wbtvU5Iq0ZrRjgIc+V/BiXBCo96KdiYAiKuaYiiJcQqSTeBIvRpi+T7ZKBJbxg0MzVCVpDlP6sBD+d4tt2G1KTH8Tc63jXH7jixNtDGxCIdP5t/HZ7+6jzZFngLjRHnV0gxo4qIS2+S+NyixGt824qSqZMQ2zUGWMA4oKS/H3qk3+c5dsUvKsJoaYf0X5D9bTt0nn9Lnogvpft4UwjjEMRlpm2aC7wA04WSRsv7qt9/itctmU9rs4zkxTpjDiXwFSF00kpsSeiCZ06i5miclFiWgaUSKAhr9JqyddqPT8BPpdMjBpMq7Q7o4sUcfMj5+dSWr/+tPrH3xeXaZfCFlA4cQR2Lx/0NAMw0QoQyJCyj7lhgjZmMT5zI8P3MO2TffprUnykVeATM/BjA7FmsT0AQoJR3K2QSk2LKU+dt+TI0V0O7EYex49vk4nbtusBplzGES6Yw8Lkdmxcc0fLGG8oFDICVK8P840ArBx0jWkdyo7fLhY0/x3q8eosyOSYW+uqITBjixr3HJVdDEPQ1wAprQCE9UI8uU9VYQ0BDnKD93Er3HT1RAIjmX5k5HA70W8cliCVGWTOo4UqxHRGGA7Uj/YNse38I9EwErIY9aNyqrd6lfWcGzM3+Bu249JdL4iI11uRLEJQkoeObHElcVQNVlBQD521FxIpNpou3IE+kz40q1wlCTiFiU9Aockz1bjkgDv/QcVOGwhMPJYsmrtm0m+BagmcaIXqCSTOFcNoQ2f7vpbla+uICytE1RPkcsNxv6pEKxrryxNAFNQRBXNTHOjUNCOyCOXOymHHG/ndjjznuw25crhRBp3IpcLBfifAPNSz7Cqq4mbt+Gkp13w0qXEavL5rBC12RNyZ7b+Nh60LTpW9DADJG1rRQrXv8Hz98yn5IwIBXnsKMchAFFgY/nC3fKq6V5LdTDgCbuZEuVQB5pAfh5nz5XXEGHY4drYhEwLGkYWw5N777Bsrvm0fTeItK+T97zKOm3Jzuc+zNaD/o+MVmsMGUU3KS3at6/bQD8VqBFtgFN4pIE/6A+4HfXzKd56XJKPfD8jIJA5FOSy6m1WXEeN4xwQ1/jmrzX0A9TFYjLhn4zfrceDLpjHm73nkqADd9yaHrrdZZMmYRTuYZUug22dugDspk82S7t6Xf9dZTu+0O0CW9J4zkBSkjzNrK4bQCarGcAVoqFT7/Kgt/8hdauNuMo8XO4QRYvzGNFOQXKCXyNbZoICiRXwEPimlQKMflsPaWHHMoe111D5MjCCAAucUM97/3sXKw3FpAubaPCpMhEJrraZJtrCPfZl/633oPVtkwXSEBTj3WS5rNSm2/Hd78FaCL3WOpKrm1TV1HHgzc8RFRTR7GFcrJUkCUVCnDCz7IKmheIleVAsmYYURRIpvSJrRyeWGLkka+vpev4MfQ5bzJxLF0p4/pNby/k4wnjSedDzdAiEWmj2TbZUhamIbboe8tdlB14oKE/yvl81n3yCe169cAqbaOPOaqEmHr3Xz2+FWhRMipg2w7PPPhX3nnhPdp4lrqhAJQKcngCllpbDku5WogXGJcVTiYgOqFP3JxRzc1Lu4S11XSb/DP6nDEukXqE+3nUvvQ8n17wM1KWpzftREYGd1Va8glji2zGp9tVV9J52MmIj2q6si0+vPd22nfrSecjjiOO8kg2Me66XUGTmtzCcmxWfPg5j9z1FJ5vkZbVD4Re+AqUF+XV0twoixPkFbBUILEsjxPnwPfxsz4dBvSn64DdqHjycXLLV9Dz/AvYcewYIqEl8s9yqHn1BZZMnUhpJMRXbjrCt6UE85KpoohcLkPvG26k/WHHECex0LJsVt50NVWLFjPwjnuV8BqXNtzyXz223tKSwBCHFo/c/TRLF63Q4G+HFm4YY6tb+ga0MI+rMS2LJ1bnG05m+82EgU/fU47jeyNPI8g2sPDMsWQ/+YxeF1zIzmeMJEQShbigQ93LL/HhRVPwrJhUZCSiEAdHEQgIMk3kduzJoDvn43TrRaTjWka//XT2Fax++EH63XgjnY4eZmZDTBnyLx/fAjTJZjbvLvyQ3//mBYodD4dmrMDDEz4mLqpBX4htoMlArM2OmtUlvbxPNt9M/9EnsMvJJ+uFN1esYeH5E8l8tpw+k6awy+jTiKSC0FacS+0rr/DxhVOx/LzOCbmu9DcFvCxWPkNd7LHDpVfQY/gIjWMqJlkednM97587keCV17AP3IcBd84nLmoFUpH8y5BtZY9AOY/tkMuGzL/zKb5YVUexaxkFIxQlQ4J8gKNuKjHLgOZEOZw4Q8rPaybsMnhPDrpsMjhmUiizeg2vTZlCfukqdvrZuXzvjFOTUavEPV95hcXTp1DefwC1lVWEyz4DcsoX7bIO9BhzBj1OGW0GAyVzCk2xPWoWvMgnkydTHEJjFNLn0pl0Hnaiku6t4W7/tKUZ+U8rQqMcOB4vv/gBf3r6TdKplALlhjJjJoRW6kyxNsmUom4EuBLPwry6qxVmtIA//OJz6dR/dyIB2XbJrK3g1fOn0/zZavqeP5Z+I0/VLKsEQWLaKy/y96lT6H/5TMoGDWH9wjfJ1FSQbt+BDv32prhPH+ltGV4Xe+BYWr++O+MCcs/9hVRpKX4mj7NzP/a+63Zo26GlP/GvGNyWQdMaTlK+oyNRtuXLBIaq7w2NPnfe/WeqqnN4rrhijBfICov0Y+iEWJn8LpzM8U3NKXHNyuZo17cLx14+3owiCM1zPZor1/DS5Blkl63mexPH0e+0EUZmUve0qH71VRZMu4ABM2bQ/bgTvnKfspgKmtqaKJYOFU/+nmXXziEtXXgrDVae5mxI359fRtcThmkW19sUFVN7ClKdSAm2eTryjaCZYRU5R5KghYFbLs++8AH/+Zf3SKdLsOMMnliUGIWUSWppKIk1VmdcVX+P8kT1GXbbfy8OPn+Ema2VfqVj0Vy5lr9NvYTs8lXsdu5Y+p16ogFNLc2i6rUFvHLRVPaefiE9f3ICgXyWViSSDUVOEl4m55Iq2KL69df54IrZFNdW4njFRFYRFjn8XJNa26C5t+C0a2fIrybRMAFwy02ZbwCt0PWWUiVpUsjF1+a49b6/Ulcf4sk1RkJMZZGESmQVMPNjWL4BMcQS8KKYsKGZvQ4awMHjfyKzQQlBhabKSp67aBaZFavYY/xodj9lWAKarJpN1YI3eHn6DAZNn0bPo4/W7CjjWiociPzbIpLHrPnz03x02x04tXXaLhTrCST2KYUJqAsC+iXgq3FaRmEx9bScc/OdrC2Cpo0RtTAjx4jgh5Xmjy98xNMvLKIkXWTUCsmQoay4vD6jsU10f0kGql4UlFqVhmKCpmb6H7Anh449xlycYaA0rVvHMxdfS9OKCgaM+yl7jThWM5yRcV0F7dXpM/n+tCl0O/aIr4YhP0Ptp5+y9PEnqXnuBVJRjpRbqm8PZRW1NSONHYd8TlSUnTngxl/itmpLiOh0YnGuGdPaQpD7J0CTaxbyKBJ2RFVdjuvuf5ma5oASW2q/Qs0o8UximLinaZaodWl9KT2BSCUhKW385iYGDt2Tw8ccZdxTQXNpWlfFHy+9mcaVqxl81nD6n3i0KYV01V3WLnyTBTOvpqz3jrTvvyftu5VTXNaKbF0DtZ9/Qc3ypdR9/DGp9XWkioqIXNf02M2Am15DYEmLWmhKTHPgs/f0aXQ58nBC8RIJnclMyVaDZsCWi3YMGXQcnl6whCee+5CidApPNX5pjJhJbdXJxMKkn6nFtwFL+pvqqqGZtc1nmth7v9044owENL0pAa2GP8y8ncZVFQw54zgGDP+xqR3Vwl3WvvkOr111HVZelBAfS4K7eKYfYvshrsyEpDxc29HwENqxyHsqOQnFFVXXMADTZA6as7TdeyD7XnMpjkxfqnAuFrnlXunmLS2Z3QxlElvd1KGuKeKaR15hbU2GIlf0fDP+ZZq+wvyFWpjegcQ3E/iNrC2AKWhRTC7TrKAdPerHJvhKMLdsGtfV8cSsu2hctYb9Rh3F3sMOTUCT9OpR+eZ7vHDNjXgSd2SRpI4VhiEStzRs8DVuyYi8fK5kUsn18rsM2SgRVjuToRthBBGNoc8PLp1Gt6E/2LBAZi51s8c3JAJRMgztsEjxzMLVPPDcuxQXpXClHSdgSSdJh1hyeBJkQ+Om0g63ReaORd2IcJSOGP0saMoxcL/d+MnpPzKxUpbdtmhYX8ejs++ntqKSA0YewT7HHWCaLDKQjMcXb73Pc7+4k7RqxEYmV8sodNwl/mkmNEN+LWWlNK5b5nPNnImiYkdkmvJ0P2QoQy+SHoTOMH1jlfANMU064I66QS6MueaRv/PRmnpKRJ8Xt1R6IQnAAKRtuNjWACylktShkWV4mit7KPT1AlqeQQLaqYckoBl5p6Gqnt9e/WtqVq/joNMPZ9+fDDVzGSpbu6x+ezHPXn8PaZkN0ZEEoRkyVmXagKr+Jg3oliGaFj4h12ZKP5G0TLEeEUSQKynmmMun0rbvDipFFbr2mzO1zYKm1yAXIKC4Dm8s+YKb//g+lltMSoir7mMSQESaMZqVumUsbhtRHIjk7BHYxnXE2kDoSETQ5DN4n1049tSDDGjJzQhoD1/7EDUVVRzy00MZcsx+xtKUcbiseGsxz9z0IGkrrYqtacSIF0QaW83OFtHeEmKqGzwKmaAAgTin4X1GDrdoas6wx/GHsc/o4YkUtWWVcougGVIrhC/FrU++y/NL11KSKiYtwV7jh4UbJ6qGqhECovaTKJJySoKxJYlCak85lyipAlqefQfvwvGn7P9l0KrreeD631JTUc2hIw5hv6P3TUCTgO/x7l/f4IX7n6bUkyaxZLrCEI2ZIJJ4Ke5qRrYKjH6D9FOQg8TSBDDZLqSpLpen1Y5dOP7SCbilRaZ020I/YcvumfDZ1TV5Zj3yFlWRT8qySEmNSV6DbpQPcPK+CRFxTFr6kpZF2pX5M7EC6XPGuKGMfUrQD/EzOYZ8f2eGnTT0S4mgvrqee296nOqKKn580kEMPWIfU1QbZ+T38x5l2ZufUOwVmVaftv/MPIiJX2J95jEzilpoM35ZMxPQ5MckEN2hRSbOcvSE09lpn92SkLD5TLDlRCDp3nH5/Ruf8cDLyyguLjIxTJh4HGqh3be8NT1KHVVNJaWnichmsny0olG5nXSkpJ2mjWCVniNy2WZ+MKgvJw7fbxPQGrjn1ieoqqjhqOH7s/+PBxNHvlrZmqWVPHTbw9g5BzOsJU0asWLNhUmpJ3QncdPEw0TZLSQEhVBDmxkrFZlO7kdCSHOumX5D9uCYc4Yl2ya3FjRphkUOVz32d95b00Qrt1iDfmiLkmFh5eo557jBHLhzp5bIIR9VUdPMNfe8gI9Dsdy07tkUedtkvHwmw9C9+3DSsE1Aq2lg3tw/UF1RxzEn/ID9fzzI7MPLRTx231MsfX8FJV6p6X5JApAMrYN+WvQaqysMzuj/Rg7fWJwtxLNkHkAVYdlkFEQ+bduXcPqFoyhuV5yQ6q9PBVu2NMtmRXWGmY++Q4OoGNIRMr1uQ+KzjUw8ZgD771Iu3dtkgM5jTVUts+5/lSgqokj0eGSmNuF1sa+gHTiwDyedsK+WSWoBlkNdbRPzbvsjVV/Ucuywfdn/RwPJZX2eeewl3lm4WOOpDv5tbE06DLgRTUiAM/HYUA8FyIzMJTKTuQt9NkE5jh2COOTkc46l9+47JOXbVoHm8OLHa7npT4tx0sWG5CaztJJZnVwz5x09gAN27SyT+1oMSytvVVUNsx9YoMV0LPP8sVQG4IVG7/ezGYYO7MUpx++jgGqNi0d9bTN33Pln1lXWcNSRg9lp52786em/sWZZNUVuSqd/DGAGEI1dBf5u0v1Gc21monxDGWWeN0kgyam6Y9n8LnGzMZdT6z702KEbdLyvwe0bLM3hzucW8x/vVRlCK622wnZpucBclolH9eeAXcvVQdxYEoLLyuoGrn7gDaIgJPBS6paSBHR+IwzxsyEHDd6JU47aK1lRU1vWVDcyd96zevFtizwVDLO5PEWeZ8ZNEyuTBRM7kfLIDL+YJGCel982uOnGKaBgcQq6dP30XzKIb0FWuvq79mLU+KNM/2AzPZfNg6bX4nDlk+/y6rImios8vDhvVAK9xgg/l+fcQ3fl8L26mhIxufC6bMjVv3mdivUZSrRxbKo6N86rYwvlOPXIgfxocB/lRaoKOzafr6rktl+9RGhbqvQ6kaPtNx1H0NEFY2kF99wwDW4EMTNNXtik8vX7olqkxcJmURUcxYci8n5A564dGXveURQVy8DI16P2jaBJEnhtVY502sPTW94wl5bJhYz4fhdGH/g9NecIVy0Cx+HJBR/x0POf0Kq4LbbcnVpaTHMuT9+2LlNG7k9Za89cV+hguRZvvv8ZDz/xDrbn4mmGTkbmlTQnYCRuaWJbQjOSkfoWi0sW0DjvV4+WMsosZRLbLPwgT3mHVpx97jEUt0pvNhl8o3ve++w7PP5eNcWlrXQlVSkQ3cyCvB+zc1uLK04ZTJtU4iYJu8+GIQ/85QNe/qBCiidsOyIOQnq1SXPmUYPp16e1UgUJwDqb5jg8/Kd3ePXNZRSlPcPpNH5KhpRYuAG0lgDfEscM8zevSSysxUoMcIkjGxgTcluIZvI5keWQy2TYZad2jBlzDI6bDChuTUxbsr6RWb97k3rfxitKJ9KwCQoSiINMlglH9OOw3cshzGvLTAb7rGRU4K0lq3jjo/Vk8jG9OqU5eGAfOpeVEErH3UppJpa6c21tjpseeJH6Zp+U1O+xpdqXKcuki2W2XLSM0G/CxwwyG7lokjW1JZyUnS3IaSbdsBdLZaIgJsz7nH7yUAYN2DGxsn/RPVuyMw4vLFrNgy8t4vOci+uWUCSyigiQsU8ugN5tPWaOGETHEotIY4+p7QQ3o/omOmOyalEkbTfZ4iMWKECnuP/p93j5neWkS4rNZGRLQJcsGWl9u/GovHFX46JmQk4eMFpZS2BT7paMlSamJrFLsrB5tUUgtbCfpyRtccjQ/hx+yO4mLGx5c9vmv8xE5/qlLWsLX2vg2cUV/P3TaqrqMjRHrka4lGeTyUcM6dOWqUftRVnajLRv0Fd05GdDjtftJqYJItxMQH3+7RX89q/vE9tp6a+0ZMHC8HKBOhTKJZ1tVPcrgLZxpjOErGWwSgBKqIlKDFFEPsjr82nHpn1pmt37dmXwwL706tVeKw2TnTfM8G7qoVuMaVGyi0SaDY4xG2qbcyxZ28SKdY2sqG5mReV6ahoyVDeF9OvenrMO2YVdOrdqCRubFiNySsmIcvgh/Hnhcp567ROyUtPabjIKbyzNWFEBnEQjS5i/9DaNZmY2ZRQUmZbZaQFK0ZbumUw2oW28slKPruWd6NKlHd26tKVHtzI6tDECgAwfanfAdAw3e3xDwW6kbvlU0fqNTCv9wQ2rUJvJU92c15i0uqqJoijLPn070qlDmRl/UiUhcdmEGa2pz/PBZzW89sFK3l6+njhdRNqRfqk5rxJXJVWiXBjLky5WIuZoInIdmbkFz7FJO8biPReK0w5FKY+S4jSlJcW0KS6iVasi2rUpolNZKe3bpCkpkYK/kB6MBFZIF0aX06C62eGYLYCWaPOkzem1cJbL3ABY8nUkm4hPEiNkm+IGi9p4yWQll61ez+eVDeRiR2VniYEyxJIPcvhJ1SAuLluC0p58vrihTzrt4rkypBeT8jy1/qJUihLPo7S4iOISS0EUlUXeu9kjUXhNDJGNIoXvODLMQDW+LWxv+wZLi2lqqKeoqBjbK6KhsZEgl1GNzUu5tC6TQj0mknkznccw/QTZF61se9M51yRBGPALK/ltN3xtHPyNLWoiaomryVZtAT6JrdpU1kRT+DKowrW0bFNLGjBm39U/H9OkgK6p5eeXzODkEcMZOmQoM6b/nKp160h7FoHvs/vAwZw1/mxKW5WoOxVm+/XbqbQlVijs5P8EqFBnEJIEW0gQqmWYgYdkb9QGucK4qdlN11Jpqi5npP4EqEKOVlfbOBGYfQTmejZia5qEjOQtexP0VFqZyOtNSDG7Xb4GtCgKNmwjLZxTz+BQta6SsWecwdnnjOeHhx7KiGHDOezQw/nh4Yex4rNPuPWWWzlnwgSOG36S2fWmnenkK76SxoZMFzU31NPY2EjH9h2w0zJ1bS6xdv068jmfDp06aevN7H4x2OiNywVrlix0hyzqq2tUmWhTVpZkX6iqXKvfmtCxvFz1v1B2xWhQtFlXuQ7PcSjr0NHUuQoW1FZXqa7XvmN7PC9trM6yqautprGhgfJOHfGKSnVxNLa2MACZJNgCaNXr1zLh7PGMGTeWAw4+mNNGjGDCeRP44ZFH6wefNeo0Bg0axHmTLtAegah6N15/A506dWTkWWfx24ce5KNFi6lrbGDtukr69OrN5GlTadehA/fNv5s3Fy7Uka1Wpa2YfvEMamtqmT9/PhdMnUrnbt25fe4tFBcXM2bs2fzh8Ud5/bUFVK+vokevXlx+1VV8sepz7rr9DlauXIFt2XTp3JmJkyfhpjxuv3UunuOybMUKss3NjDjpJH4y4iRyTU38+v77ee2VV9Ta27Zty9QLL6TXjn147OGHefFvL1BcUkIQB4wcPZq9Bw9RS9QtlbqIW5pPk9GmBLQzx43loEMOYdRPT2XggAHs84P9WLp0KY8/9hgXXDCFw48+jlj0tAjOHDmabl27MOeGm7j68ktZ8OprzL5mDn4+z8zLLmP69BkM2W8Ixx97HIP23pvx501g/fpK+u2xF2+/9aa+5t5776V3312YMH4srVu35tpf3sQvr7mKl198iVmzZ9GpWw+1hEsvnk5tdQ1Tp8+grrqKK2ZezuWzZ9Fzh16cctIIDj7wQEaffQ6PPHAfiz9YxCOPPspTTz3JPfPuYvr06fTcqS8f/+Mf7DtkCBWrVnLJxRczccJE+g3Ym/vumcey5Su4fd5d2q03TMC46lcSgRnYM3GgoWY9488ay+izxvDDH/2IM0eN0u51u44dsD2Pwd8fzPATT8T1zKYtoQrnjBtPty6dmTnnWq6+/DJqq6q4/rY7aKirYszoMzjt9NM5/sSTeerxR/njH/5AaatWHH3ssRx2xJG8/urLzJkzh7vuvptuvXbkoinn66rPuvpabvrFHCpWreb6ubfrta1YuoSzx41jxvQZHPLjI6mpWsv4cWcz7aILFbRRp43koqlTOezoY3n0Nw/x6G9/y+8ee4zrfnk9a79Yw81z7+g50sMAAAM9SURBVPhSfP/Tf/yBu++6i/2GDNH00FBfT9b3ueSymeraGw8AtoC2cTj7YnUFrdu2Zd3aCiZNmMikKVMYeuABunpnjBzFcSNOTuJvMpufzEHINPW4s86ke5duXHHtdcy+9GJq1ldx07x5VK6p4JxxZzNq9Jkcc9zx1FRVqsj0X8/8J3fOm8fNN9+iG8GmTpvGbXPn0nunvpwz7ix679hHQRNLW/X559w8d65m6tUrl3P22LGMPPU0Th41mnfeeJ2ZMy9j1pVX0r1nD318yuQpHHXCcH5z/7088cQTPPrYY9x7/3089+xfmTt3Lu3Lu1CxahUdO5fz9sLXuWbO1fz84kvoP3AgNdXVagydu/f4Cl/7MmhJS+v6a6/lH4sX64bUYi/NVXPmUNyqhHFnjmHk6adz1PHDN8jUG58yipg8cSJdOnfl4iuu4Lo5V1FdWcW1N9+oLnj+uRMYM3Yse+6xu7pS27I2OiOyanUFl8+aRXnnzkyfNo3mpia6dunK8pUrGLTP95l+8aXc+svrWbVqNdfd8Est60QUuPv223j2mb/Qe8feRGHEFxUVzLjkErr17MG4MWOYdP4kfnjkkTz24EP88T+eYv6991JdV8uVs2eRaWiirEM7amprmTx5Mv327M+Nv7iWT5Z8wg69d2B9TRUHHXQQw0b8NCHnG2l1he9PK5S5knbXVVby6ZIlItzQb5fdaF9eThDkWLlsOR3bd6RNexmEM/s8k2SX1Gsxqz9fheelKO/WlTUVX+igSvdevQjDwLy/c7nGqRXLlrNy5UoNrH137kvXHj3VXWrWr2PxokW0bVtGeedygjiie/ceVFZUkM/79NihV7IJ11ZleNG771HbUE/fnXfWz2jfvgOpojTLly2nW7eu6v5i7XV1dfTaYQds16OxoY5/LFqsu/y6du/Gjn364MnQXxiw6IMPqFq/ntZtWrHLrrvSuk3bL4uRm00ESVpucXpN1QWuJaZlWmZfexTeu1F6N6l+k/d/3WckVOcr5930XIUXqGdsyqM2Ic1f995NP1sJue4F+uotfc29/tODyptH6f+/Z/4N2las+b9B+zdoW4HAVrzl35b2b9C2AoGteMt/A4q+//C6EjopAAAAAElFTkSuQmCC" /><br />
  ขอแสดงความนับถือ<br />
  บริษัท เจพี ประกันภัย จำกัด (มหาชน)<br />
  <br />
  อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อตามเบอร์โทรศัพท์ที่ได้ให้ไว้ด้านบน<br />
  <br />
  <a href="http://www.jpinsurance.co.th/" rel="noopener noreferrer" target="_blank">บริษัท เจพี ประกันภัย จำกัด (มหาชน)</a>&nbsp;หมายเลขโทรศัพท์ 0 2290 0555</p>';

}




function mailsend($arr){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);

  date_default_timezone_set('Asia/Bangkok');
  require '../../../PHPMailer/PHPMailerAutoload.php';

  $title = '(OTP) '.$arr['ref'].' : BackEnd [System] By JP Insurance Co., Ltd.';
  $fullname = $arr['name'].' '.$arr['last'];

  $date = DateTimeThai($arr['date5m']);

  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>

      <p>สวัสดี&nbsp;<strong>'.$fullname.'</strong><br />
      <br />
      บริษัท เจพี ประกันภัย จำกัด (มหาชน) ขอแจ้งรหัสผ่านของคุณให้ทราบว่า<br />
      <br />
      รหัสผ่าน OTP สำหรับระบบ <strong>System</strong> ของคุณคือ&nbsp;<br />
      <br />
      <span style="color:#e74c3c"><strong>'.$arr['otp'].'</strong></span>&nbsp;( รหัสอ้างอิง '.$arr['ref'].' )&nbsp;<br />
      <br />
      <span style="color:#e74c3c"><strong>*** รหัสผ่านนี้จะหมดอายุเมื่อ '.$date.' ***</strong>&nbsp;</span><br />
      <br />
      '.signature().'
      </body>
    </html>
    ';

  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row['data'][0]['mail_host'];
  $mail->Port       = $row['data'][0]['mail_port'];
  $mail->SMTPSecure = $row['data'][0]['mail_SMTPSecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row['data'][0]['mail_Username'];
  $mail->Password   = $row['data'][0]['mail_Password'];
  $mail->SetFrom($row['data'][0]['mail_Username'], $title);
  $mail->addAddress($arr['mail'], 'ToEmail');
  //$mail->SMTPDebug  = 3;
  //$mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";}; //$mail->Debugoutput = 'echo';
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;
  // $mail->addAttachment('../image/Preloader_3.gif', 'Preloader_3.gif');

  if($mail->send()) { return 200; } else { return 404; }
  // $mail->send()?return 200;:return 404;

}

function getDataSSL($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function FIX_PHP_CORSS_ORIGIN(){

 //http://stackoverflow.com/questions/18382740/cors-not-working-php
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

}

function getMunu($arrPage)
{
  $sqlp = "SELECT p.*,m.root_id,m.module_name,m.module_icon,r.root_name
           FROM t_page p , t_module m , t_root r
           WHERE p.page_id IN ($arrPage) AND p.is_active = 'Y'
           AND m.module_type = '1' AND m.is_active = 'Y' AND p.module_id = m.module_id
           AND m.root_id = r.root_id
           order by r.root_seq, m.module_order";
  $queryp = DbQuery($sqlp,null);
  $rows   = json_decode($queryp, true);
  $rowp   = $rows['data'];

  return $rowp;

}




function updateDataLog($arr){
  // ut_event = INS , UPD , DEL
  // ut_topic = table DB
  $sql  = "INSERT INTO uplate_time VALUES(null,now(),'{$arr['ut_event']}','{$arr['ut_topic']}','{$arr['user_id']}')";
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];

  if(intval($row['errorInfo'][0]) == 0){
    return 200;
  }else{
    return 400;
  }
}

function showUpdateLog($event,$topic){
  $sql = "SELECT * FROM uplate_time WHERE ut_event = '$event' AND ut_topic = '$topic' ORDER BY ut_timestamp DESC";
  $query = DbQuery($sql,null);
  // $row   = json_decode($query, true);
  return $query;
}



function getDataMaster($dataGroup,$datacode){
    $dataDesc = "";
    $sql   = "SELECT * FROM data_master where DATA_GROUP = '$dataGroup' and DATA_CODE = '$datacode' ";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $dataDesc = $json['data'][$i]['DATA_DESC1'];
    }
    return $dataDesc;
}


function getVendorByUserId($userId)
{
    $vendor = "";
    $sql   = "SELECT * FROM t_vendor where is_active = 'Y' and user_id = '$userId'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    return $vendor;
}

function getoptionDataMaster($dataGroup,$datacode){
    $optionMaster = "";

    $sql   = "SELECT * FROM data_master where DATA_GROUP = '$dataGroup'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $code         = $json['data'][$i]['DATA_CODE'];
      $dataDesc     = $json['data'][$i]['DATA_DESC1'];
      $selected =  ' ';
      if($datacode == $code && $datacode != ""){
        $selected =  'selected="selected"';
      }
      $optionMaster .= '<option value="'.$code.'" '.$selected.'>'.$dataDesc.'</option>';
    }
    return $optionMaster;
}

function getoptionBranch($code){
    $optionBranch = "";
    $sql   = "SELECT * FROM t_branch where is_active = 'Y' order by branch_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $branch_code    = $json['data'][$i]['branch_code'];
      $cname          = $json['data'][$i]['cname'];
      $selected =  ' ';
      if($code == $branch_code && $code != ""){
        $selected =  'selected="selected"';
      }
      $optionBranch .= '<option value="'.$branch_code.'" '.$selected.'>'.$cname.'</option>';
    }
    return $optionBranch;
}

function getoptionVendor($id){
    $option = "";
    $sql   = "SELECT * FROM t_vendor where is_active = 'Y' order by vendor_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $vendor_id    = $json['data'][$i]['vendor_id'];
      $vendor_name  = $json['data'][$i]['vendor_name'];
      $selected =  ' ';
      if($id == $vendor_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$vendor_id.'" '.$selected.'>'.$vendor_name.'</option>';
    }
    return $option;
}


function getBranchName($code){
    $optionBranch = "";
    $sql   = "SELECT * FROM t_branch where branch_code = '$code'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $cname = "";
    if($dataCount > 0){
      $cname  = $json['data'][0]['cname'];
    }

    return $cname;
}


function DBInsertPOST($arr,$taleName){
  unset($arr['button']);
  $arrKey   = array();
  $arrValue = array();
  foreach ($arr as $key => $value) {
    $arrKey[]   = $key;
    $arrValue[] = "'".$value."'";
  }
  $strKey   = implode(",",$arrKey);
  $strValue = implode(",",$arrValue);
  $data = "INSERT INTO $taleName ($strKey) VALUES ($strValue);";

  return json_encode(array('status'=> 200 , 'message' => 'Success' , 'data' => $data));
}

function DBUpdatePOST($arr,$taleName,$pk){
  unset($arr['button']);
  $arrUpdate = array();
  $where = "";
  foreach ($arr as $key => $value) {
    if($key == $pk){
      $where = "WHERE $key = '$value'";
    }else{
      $arrUpdate[]   = "$key = '$value'";
    }
  }
  $strUpdate = implode(",",$arrUpdate);
  $data = "UPDATE $taleName SET $strUpdate $where;";
  return json_encode(array('status'=> 200 , 'message' => 'Success' , 'data' => $data));
}

function DBInsertPOST2($arr,$taleName){
  unset($arr['button']);
  $arrKey   = array();
  $arrValue = array();
  foreach ($arr as $key => $value) {
    $arrKey[]   = $key;
    $arrValue[] = "'".$value."'";
  }
  $strKey   = implode(",",$arrKey);
  $strValue = implode(",",$arrValue);
  $data = "INSERT INTO $taleName ($strKey) VALUES ($strValue);";

  return $data;
}

function DBUpdatePOST2($arr,$taleName,$pk){
  unset($arr['button']);
  $arrUpdate = array();
  $where = "";
  foreach ($arr as $key => $value) {
    if($key == $pk){
      $where = "WHERE $key = '$value'";
    }else{
      $arrUpdate[]   = "$key = '$value'";
    }
  }
  $strUpdate = implode(",",$arrUpdate);
  $data = "UPDATE $taleName SET $strUpdate $where;";
  return $data;
}


function getoptionDepartment($id){
    $option = "";
    $sql   = "SELECT * FROM t_department where is_active = 'Y' order by department_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $department_id  = $json['data'][$i]['department_id'];
      $name           = $json['data'][$i]['department_name'];

      $selected =  ' ';
      if($id == $department_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$department_id.'" '.$selected.'>'.$name.'</option>';
    }
    return $option;
}




function getoptionProvince($name){
    $option = "";
    $sql   = "SELECT * FROM data_mas_add_province order by PROVINCE_NAME";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $PROVINCE_CODE  = $json['data'][$i]['PROVINCE_CODE'];
      $PROVINCE_NAME  = $json['data'][$i]['PROVINCE_NAME'];

      $selected =  ' ';
      if($name == $PROVINCE_NAME && $name != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$PROVINCE_CODE.'|'.$PROVINCE_NAME.'" '.$selected.'>'.str_replace("จังหวัด","",$PROVINCE_NAME).'</option>';
    }
    return $option;
}


function getoptionDistrict($code,$name){
    $option = "";

    $sql   = "SELECT * FROM data_mas_add_district where PROVINCE_CODE = '$code' order by DISTRICT_NAME";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    $vowels = array("อำเภอ", "เขต");

    for ($i=0; $i < $dataCount; $i++) {
      $DISTRICT_CODE  = $json['data'][$i]['DISTRICT_CODE'];
      $DISTRICT_NAME  = $json['data'][$i]['DISTRICT_NAME'];

      $selected =  ' ';
      if($name == $DISTRICT_NAME && $name != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$DISTRICT_CODE."|".$DISTRICT_NAME.'" '.$selected.'>'.str_replace($vowels,"",$DISTRICT_NAME).'</option>';
    }
    return $option;
}

function getoptionSubDistrict($code,$name){
    $option = "";

    $sql   = "SELECT * FROM data_mas_add_subdistrict where DISTRICT_CODE = '$code' order by SUBDISTRICT_NAME";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    $vowels = array("ตำบล", "แขวง");

    for ($i=0; $i < $dataCount; $i++) {
      $SUBDISTRICT_NAME     = $json['data'][$i]['SUBDISTRICT_NAME'];
      $SUBDISTRICT_POSTAL   = $json['data'][$i]['SUBDISTRICT_POSTAL'];

      $selected =  ' ';
      if($name == $SUBDISTRICT_NAME && $name != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$SUBDISTRICT_NAME."|".$SUBDISTRICT_POSTAL.'" '.$selected.'>'.str_replace($vowels,"",$SUBDISTRICT_NAME).'</option>';
    }
    return $option;
}








function monthThai($month){
  $arr = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
  return $arr[$month];
}

function monthThaiFull($month){
  $arr = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
  return $arr[$month];
}

function getRoleName($roleList){
    $RoleName= "-";
    $sql   = "SELECT * FROM t_role where role_id in ($roleList) ";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      if($i == 0){
        $RoleName = $json['data'][$i]['role_name'];
      }else{
        $RoleName .= ", ".$json['data'][$i]['role_name'];
      }
    }
    return $RoleName;
}


function resizeImageToUpload($obj,$h,$w,$path,$title) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {
    if(getimagesize($obj['tmp_name']))
    {
      $ext = pathinfo($obj["name"], PATHINFO_EXTENSION);
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = $title.time();

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }

          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);
        }else{
          $fileinfo = pathinfo($obj['name']);
          $filetype = strtolower($fileinfo['extension']);
          $new_images = $title.time().".$filetype";
          move_uploaded_file($obj['tmp_name'],$path.'/'.$new_images);
        }
      }
    }
  }
  return $new_images;
}




function resizeImageToBase64($obj,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {

    if(getimagesize($obj['tmp_name']))
    {
      $ext = pathinfo($obj["name"], PATHINFO_EXTENSION);
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = "thumbnails_".$user_id_update;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }


          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);

          $image   = file_get_contents($path.$new_images);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }else{
          $image   = file_get_contents($path.$name);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }

      }
    }
  }
  return $img;
}


function formatDateThtoEn($date){
  $Datestr = "";
  if(!empty($date) && strlen($date) >= 8){
    $date = str_replace ('-','/',$date);
    $dt = explode("/", $date);
    $d  = $dt[0];
    $m  = $dt[1];
    $y  = $dt[2];

    if($y > 2500){
      $y -= 543;
    }
    if(strlen($d) == 1){
      $d  = "0".$d;
    }

    if(strlen($m) == 1){
      $m  = "0".$m;
    }

    $date = $y."/".$m."/".$d;
    if(checkdate($m,$d,$y)){
      $Datestr = date("Y-m-d", strtotime($date));
    }
  }
  //echo $Datestr."<br>";
  return $Datestr;
}

function formatDateTh($date){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date("d/m/Y", $d);
  }
  return $Datestr;
}

function DateDiff($strDate1,$strDate2)
{
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}

function TimeDiff($strTime1,$strTime2)
{
		return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

function DateTimeDiff($strDateTime1,$strDateTime2)
{
		return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

function chkNum($obj){
  $obj = str_replace(",","",$obj);
  if(!isset($obj) || empty($obj)){
      $obj = 0;
  }
  if(!is_numeric($obj)){
    $obj = "0";
  }
  return $obj;
}

function mailsendAttachment($title,$message,$addBCC,$addAttachment){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true)['data'];
  // print_r($row);
  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>
      '.$message.'
      </body>
    </html>
    ';

  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row[0]['mail_host'];
  $mail->Port       = $row[0]['mail_port'];
  $mail->SMTPSecure = $row[0]['mail_SMTPSecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row[0]['mail_Username'];
  $mail->Password   = $row[0]['mail_Password'];
  $mail->SetFrom($row[0]['mail_Username'], $title);
  foreach ($addBCC as $value) {
    $mail->addBCC($value, 'ToEmail');
  }
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;
  $mail->addAttachment($addAttachment['path'], $addAttachment['newname']);

  if($mail->send()) { return 200; } else { return 404; }

}

function generateToken($length)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
    $token = '';
    for ($i = 0; $i < $length; $i++) {
        $token .= $characters[rand(0, $charactersLength - 1)];
    }

  return $token;
}

// function GET_LINE_TOKEN($token){
//
//   return $token != "" ?$token:'KahKOQ6nYPILYRC4iveYEruIPDMclGQst1Bf67mKz2p';
// }

function LINE_NOTIFY_MESSAGE($message,$imageFile,$token){
  $LINE_TOKEN = $token;
  define('LINE_API',"https://notify-api.line.me/api/notify");
  define('LINE_TOKEN', $LINE_TOKEN);

  $queryData = array(
    'message' => $message,
    'imageThumbnail' => $imageFile,
    'imageFullsize' => $imageFile
  );
    $queryData = http_build_query($queryData,'','&');
    $headerOptions = array(
     'http'=>array(
       'method'=>'POST',
       'header'=> "Content-Type: application/x-www-form-urlencoded\r\n"
       ."Authorization: Bearer ".LINE_TOKEN."\r\n"
       ."Content-Length: ".strlen($queryData)."\r\n",
       'content' => $queryData
     ),
     'ssl'=>array(
       "verify_peer"=>false,
       "verify_peer_name"=>false,
      )
    );
    $context = stream_context_create($headerOptions);
    $result = file_get_contents(LINE_API,FALSE,$context);
    $res = json_decode($result,true);
    return $res;
}

function uploadfile($attach,$url,$title){
  $fileinfo = pathinfo($attach['name']);
  $filetype = strtolower($fileinfo['extension']);
  $image = $title.time().".$filetype";
  move_uploaded_file($attach['tmp_name'],$url.'/'.$image);

  return array('image' => $image);
}






?>
