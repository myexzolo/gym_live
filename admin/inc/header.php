<!DOCTYPE html>
<html lang="th">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="../../styles/style.css" rel="stylesheet" />
        <link href="../../styles/bootstrap.css" rel="stylesheet" />
        <link href="../../styles/chat.css" rel="stylesheet" />
        <link href="../../styles/smoke.css" rel="stylesheet" />
        <link href="../../styles/all.min.css" rel="stylesheet" />
        <link rel="shortcut icon" type="images/png" href="../../images/fav.png"/>

        <title>Gym Monkey Live Streaming</title>
    </head>
    <body>
