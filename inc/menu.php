<?php
$name = isset($_SESSION['person']['name'])?$_SESSION['person']['name']." ".$_SESSION['person']['lname']:"";
?>

        <div class="container-fluid responsive" id="main-header">
            <div class="row">
                <div class="col-md-6">
                    <a class="navbar-brand" href="https://gymmonkeybkk.com">
                        <img src="../../images/logo_yms.png" alt="gym logo" width="230">
                    </a>
                </div>
                <div class="col-md-6 my-auto">
                    <nav class="navbar justify-content-end">
                        <form class="nav-bartop">
                            <button class="btn btn-success" type="button" style="display:none">สมัครสมาชิก</button>
                            <button class="btn btn-danger" type="button" style="display:none">เข้าสู่ระบบ</button>
                            <button class="btn btn-danger" type="button" onclick="logout()">ออกจากระบบ</button>
                            <br /><a herf="#" class="btn-forget" style="display:none">ลืมรหัสผ่าน?</a>
                        </form>
                    </nav>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="main-menu">
            <nav class="navbar navbar-expand-lg navbar-dark static-top navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="../../pages/home">หน้าหลัก <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="../../pages/resetPassword">เปลี่ยนรหัสผ่าน <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="https://gymmonkeybkk.com">กลับหน้าเว็บไซต์ <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-success btn-sm btn-user">
                              <i class="fas fa-user"></i><?= " ".$name ?> | Online
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- mobile switch on -->
                <form class="nav-bartop-mobile">
                            <button class="btn btn-success" type="button" style="display:none">สมัครสมาชิก</button>
                            <button class="btn btn-danger" type="button" style="display:none">เข้าสู่ระบบ</button>
                            <button class="btn btn-dark" type="button" onclick="logout()">ออกจากระบบ</button>
                            <br /><a herf="#" class="btn-forget" style="display:none">ลืมรหัสผ่าน?</a>
                        </form>
                <form class="form-inline form-search">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-dark my-2 my-sm-0" type="submit">ค้นหาวีดีโอ</button>
                </form>
            </nav>
        </div>
