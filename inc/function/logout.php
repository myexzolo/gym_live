<?php
if(!isset($_SESSION))
{
  session_start();
  session_destroy();
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true ,'message' => 'Success')));

?>
