<?php
    include('connect.php');
    if(!isset($_SESSION))
    {
        session_start();
    }
    $status = false;
    $user_token = "";
    //print_r($_SESSION['person']);
    if(!isset($_SESSION['person']['code']) || !isset($_SESSION['person']['token'])) {
      $status = true;
    }
    else if($_GET('bypass') == "y")
    {
      $status = false;
    }
    else
    {
      $user = $_SESSION['person']['code'];
      $sql = "SELECT PERSON_LINE_SERVICE_TOKEN FROM person WHERE PERSON_CODE = '$user'";
      //echo $sql;
      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $row        = $json['data'];


      $user_token   = isset($row[0]['PERSON_LINE_SERVICE_TOKEN'])?$row[0]['PERSON_LINE_SERVICE_TOKEN']:"";
      $token        = $_SESSION['person']['token'];

      if($user_token != $token)
      {
        $status = true;
      }
    }
    header('Content-Type: application/json');
    exit(json_encode(array('status' => $status,'message' => 'Success')));
?>
