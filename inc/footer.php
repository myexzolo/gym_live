    <div class="container-fluid footer responsive">
        <div class="row">
            <div class="col-md-12">
                <p>สอบถามข้อมูลเพิ่มเติม กรุณาติดต่อ Gym Monkey โทร. 063-1975662 อีเมล์ : Gymmonkeythailand@gmail.com</p>
                <p>สงวนลิขสิทธิ์ Gym Monkey นโยบายความเป็นส่วนตัว เนื้อหาทั้งหมดบนเว็บไซต์นี้ มีขึ้นเพื่อวัตถุประสงค์ในการให้ข้อมูลและเพื่อการศึกษาเท่านั้น </p>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../javascript/jquery.min.js"></script>
    <script src="../../javascript/bootstrap.min.js"></script>
    <script src="../../javascript/smoke.js"></script>
    <script src="../../javascript/all.min.js"></script>
    <script src="../../javascript/popper.min.js"></script>
    <script>
    function logout(){
      $.smkConfirm({
        text:'ยืนยันการออกจากระบบ ?',
        accept:'ยืนยัน',
        cancel:'ยกเลิก'
      },function(res){
        // Code here
        if (res) {
          //console.log("xxx");
          $.get( "../../inc/function/logout.php")
          .done(function( data ) {
            console.log(data);
            if(data.status){
              window.location='../../pages/login/';
            }

          });

        }
      });
    }
    </script>
    </body>

</html>
